import { Application } from "oak";

import { config } from "@/config.ts";
import { routers } from "@/routers.ts";

import { initKv } from "@/lib/kv.ts";

type State = {
  kv: Deno.Kv | null;
};

const app = new Application<State>({
  state: { kv: null },
});

app.use(async (ctx, next) => {
  const kv = await initKv();

  ctx.state.kv = kv;

  await next();

  await kv.close();
});

for (const router of routers) {
  app.use(router.routes());
  app.use(router.allowedMethods());
}

console.log(`Listening on port ${config.port}...`);
await app.listen({ port: config.port });
