import { AppRouter } from "@/lib/create-router.ts";

import { rootRouter } from "@/app/root/root.router.ts";
import { usersRouter } from "@/app/users/users.router.ts";

export const routers: AppRouter[] = [
  rootRouter(),
  usersRouter(),
];
