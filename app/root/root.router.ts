import { Status } from "oak";

import { createRouter } from "@/lib/create-router.ts";

export const rootRouter = createRouter((r) => {
  r.get("/", (ctx) => {
    ctx.response.body = { message: "Hello!" };
    ctx.response.status = Status.OK;
  });
});
