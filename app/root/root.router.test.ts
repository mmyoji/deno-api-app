import { testing } from "oak";
import { assertEquals } from "std/testing/asserts.ts";
import { describe, it } from "std/testing/bdd.ts";

import { rootRouter } from "@/app/root/root.router.ts";

const router = rootRouter();

describe("rootRouter", () => {
  it("GET /", async () => {
    const ctx = testing.createMockContext({
      method: "GET",
      path: "/",
    });
    const next = testing.createMockNext();
    const mw = router.routes();

    await mw(ctx, next);

    assertEquals(ctx.response.body, { message: "Hello!" });
    assertEquals(ctx.response.status, 200);
  });
});
