type Success = {
  success: true;
  value: { name: string };
};

type Failure = {
  success: false;
  error: string;
};

export interface IUsersValidator {
  validate: (args: unknown) => Success | Failure;
}

export class UsersValidator implements IUsersValidator {
  validate(args: unknown): Success | Failure {
    if (args == null || typeof args !== "object" || !("name" in args)) {
      return { success: false, error: "[name] must be present" };
    }

    if (typeof args.name !== "string") {
      return { success: false, error: "[name] must be string" };
    }

    const { name } = args;
    return { success: true, value: { name } };
  }
}
