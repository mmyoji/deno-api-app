import { assert, assertEquals } from "std/testing/asserts.ts";

import { UsersService } from "@/app/users/users.service.ts";
import { IUsersValidator } from "@/app/users/users.validator.ts";
import { IUsersRepository } from "@/lib/repositories/users.repository.ts";

Deno.test("UsersService.create", async (t) => {
  const repository = {
    create({ name }: { name: string }): ReturnType<IUsersRepository["create"]> {
      return Promise.resolve({ id: 4, name });
    },

    exists({ name }: { name: string }): ReturnType<IUsersRepository["exists"]> {
      if (name !== "Tom") {
        return Promise.resolve(false);
      }

      return Promise.resolve(true);
    },

    findMany(): ReturnType<IUsersRepository["findMany"]> {
      return Promise.resolve([
        { name: "foo" },
        { name: "bar" },
        { name: "buz" },
      ]);
    },
  };

  const validator = {
    validate(args: unknown): ReturnType<IUsersValidator["validate"]> {
      if (
        args == null || typeof args !== "object" || !("name" in args) ||
        typeof args.name !== "string"
      ) {
        return { success: false, error: "[name] must be present" };
      }

      const { name } = args;
      return { success: true, value: { name } };
    },
  };

  const service = new UsersService(repository, validator);

  await t.step(
    "returns an error message when args doesn't have name",
    async () => {
      const { error } = await service.create({});

      assertEquals(error, "[name] must be present");
    },
  );

  await t.step(
    "returns an error message when name is already taken",
    async () => {
      const { error } = await service.create({ name: "Mary" });

      assertEquals(error, "[name] has already been taken");
    },
  );

  await t.step("returns a user with valid args", async () => {
    const { user } = await service.create({ name: "Tom" });

    assert(user);
    assertEquals(user.name, "Tom");
  });
});
