import { assert, assertEquals } from "std/testing/asserts.ts";

import { UsersValidator } from "@/app/users/users.validator.ts";

Deno.test("UsersValidator#validate()", async (t) => {
  const validator = new UsersValidator();

  await t.step(
    "returns an error message when [name] doesn't exist",
    () => {
      const result = validator.validate({});

      assert(!result.success);
      assertEquals(result.error, "[name] must be present");
    },
  );

  await t.step(
    "returns an error message when [name] isn't string",
    () => {
      const result = validator.validate({ name: 1 });

      assert(!result.success);
      assertEquals(result.error, "[name] must be string");
    },
  );

  await t.step("returns a valid object with valid args", () => {
    const result = validator.validate({ name: "Mary", extraKey: 100 });

    assert(result.success);
    assertEquals(result.value, { name: "Mary" });
  });
});
