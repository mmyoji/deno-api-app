import { IUsersValidator } from "@/app/users/users.validator.ts";
import { User } from "@/lib/entities/user.ts";
import { IUsersRepository } from "@/lib/repositories/users.repository.ts";

type Repository = Pick<IUsersRepository, "create" | "exists" | "findMany">;

export class UsersService {
  constructor(
    private repository: Repository,
    private validator: IUsersValidator,
  ) {}

  async create(
    args: unknown,
  ): Promise<{ user: null; error: string } | { user: User; error: null }> {
    const result = this.validator.validate(args);
    if (!result.success) {
      return { error: result.error, user: null };
    }

    if (!(await this.repository.exists(result.value))) {
      return { error: "[name] has already been taken", user: null };
    }

    const user = await this.repository.create(result.value);
    return { error: null, user };
  }

  findMany(): Promise<User[]> {
    return this.repository.findMany();
  }
}
