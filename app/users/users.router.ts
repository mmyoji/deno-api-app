import { Status } from "oak";

import { createRouter } from "@/lib/create-router.ts";

import { UsersService } from "@/app/users/users.service.ts";
import { UsersValidator } from "@/app/users/users.validator.ts";
import { User } from "@/lib/entities/user.ts";
import { UsersRepository } from "@/lib/repositories/users.repository.ts";

function initService(kv: Deno.Kv): UsersService {
  const repository = new UsersRepository(kv);
  const validator = new UsersValidator();
  return new UsersService(repository, validator);
}

export const usersRouter = createRouter((r) => {
  r.get("/users", async (ctx) => {
    const service = initService(ctx.state.kv);
    const users = await service.findMany();
    ctx.response.body = { users };
  });

  r.post("/users", async (ctx) => {
    const body = ctx.request.body({ type: "json" });
    const value = await body.value;

    const service = initService(ctx.state.kv);

    const { user, error } = await service.create(
      value as Partial<Pick<User, "name">>,
    );
    if (typeof error === "string") {
      ctx.response.status = Status.UnprocessableEntity;
      ctx.response.body = { errors: [{ message: error }] };
      return;
    }

    ctx.response.status = Status.Created;
    ctx.response.body = { user };
  });
});
