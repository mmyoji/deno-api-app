import { assert, assertEquals, assertInstanceOf } from "std/testing/asserts.ts";
import { afterEach, beforeEach, describe, it } from "std/testing/bdd.ts";

import { UsersRepository } from "@/lib/repositories/users.repository.ts";
import { teardown } from "@/lib/testutils/kv.ts";

describe("UsersRepository", () => {
  let kv: Deno.Kv;
  let repo: UsersRepository;

  beforeEach(async () => {
    kv = await Deno.openKv(":memory:");
    repo = new UsersRepository(kv);

    await Promise.all(
      ["foo", "bar", "buz"].map((name) => repo.create({ name })),
    );
  });

  afterEach(async () => {
    await teardown(kv);
  });

  describe("findMany()", () => {
    it("returns all users", async () => {
      const users = await repo.findMany();

      assertEquals(users, [
        { name: "bar" },
        { name: "buz" },
        { name: "foo" },
      ]);
    });
  });

  describe("findOne()", () => {
    it("returns a user when they're found", async () => {
      const user = await repo.findOne("foo");

      assert(user);
      assertEquals(user.name, "foo");
    });

    it("returns null when they're NOT found", async () => {
      const user = await repo.findOne("foofoo");

      assertEquals(user, null);
    });
  });

  describe("count()", () => {
    it("returns # of users", async () => {
      const count = await repo.count();

      assertEquals(count, 3);
    });
  });

  describe("create()", () => {
    it("creates a new user when name is not taken", async () => {
      const user = await repo.create({ name: "foofoo" });

      assert(user);
      assertEquals(user.name, "foofoo");
      assertEquals(await repo.count(), 4);
    });

    it("raises an error when name is already taken", async () => {
      try {
        await repo.create({ name: "foo" });
        assert(false);
      } catch (err) {
        assertInstanceOf(err, TypeError);
        assertEquals(err.message, "User with name already exists");
      }

      assertEquals(await repo.count(), 3);
    });
  });

  describe("#exists()", () => {
    it("returns true when name exists", async () => {
      const result = await repo.exists({ name: "foo" });

      assertEquals(result, true);
    });

    it("returns false when name exists", async () => {
      const result = await repo.exists({ name: "foobar" });

      assertEquals(result, false);
    });
  });
});
