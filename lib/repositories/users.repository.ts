import { User } from "@/lib/entities/user.ts";
import { keys } from "@/lib/kv.ts";

export interface IUsersRepository {
  findMany: () => Promise<User[]>;
  findOne: (name: string) => Promise<User | null>;
  count: () => Promise<number>;
  create: (args: { name: string }) => Promise<User>;
  exists: (args: { name: string }) => Promise<boolean>;
}

export class UsersRepository implements IUsersRepository {
  constructor(private kv: Deno.Kv) {}

  async findMany(): Promise<User[]> {
    const users: User[] = [];
    for await (const entry of this.kv.list<User>({ prefix: [keys.users] })) {
      users.push(entry.value);
    }
    return users;
  }

  async findOne(name: User["name"]): Promise<User | null> {
    const res = await this.kv.get<User>([keys.users, name]);
    return res.value;
  }

  async count(): Promise<number> {
    const users = await this.findMany();
    return users.length;
  }

  async create({ name }: Pick<User, "name">): Promise<User> {
    const user: User = { name };
    const pkey = [keys.users, user.name];
    const res = await this.kv.atomic()
      .check({ key: pkey, versionstamp: null })
      .set(pkey, user)
      .commit();
    if (!res.ok) {
      throw new TypeError("User with name already exists");
    }

    return user;
  }

  async exists({ name }: Pick<User, "name">): Promise<boolean> {
    const res = await this.kv.get([keys.users, name]);
    return !!res.value;
  }
}
