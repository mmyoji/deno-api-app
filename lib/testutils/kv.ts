import { keys } from "@/lib/kv.ts";

export async function teardown(kv: Deno.Kv): Promise<void> {
  for (const key of Object.values(keys)) {
    const deletes: Promise<void>[] = [];
    for await (const entry of kv.list({ prefix: [key] })) {
      deletes.push(kv.delete(entry.key));
    }
    await Promise.all(deletes);
  }

  await kv.close();
}
