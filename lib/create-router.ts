import { Router } from "oak";

import { AppState } from "@/lib/app-state.ts";

export type AppRouter = Router<AppState>;

export function createRouter(
  fn: (r: AppRouter) => void,
): () => AppRouter {
  return () => {
    const r = new Router<AppState>();

    fn(r);

    return r;
  };
}
