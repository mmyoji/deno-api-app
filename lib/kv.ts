export function initKv(path?: string): Promise<Deno.Kv> {
  return Deno.openKv(path);
}

export const keys = {
  users: "users",
} as const;
