import { initKv, keys } from "@/lib/kv.ts";

export async function run(): Promise<void> {
  const kv = await initKv();

  for (const name of ["foo", "bar", "buz"]) {
    await kv.set([keys.users, name], { name });
  }
}
