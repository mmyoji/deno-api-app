const dirname = new URL(".", import.meta.url).pathname;

for await (const entry of Deno.readDir(dirname)) {
  const { name } = entry;

  if (name.startsWith("_")) {
    continue;
  }

  const file = `./${name}`;
  const mod = await import(file);
  console.log(`Seeding: ${file}`);
  await mod.run();
}
